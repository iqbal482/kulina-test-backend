package main

import (
	"fmt"
	"strconv"
)

func main() {
	var numberInput int
	var result []string
	fmt.Printf("Please input number: ")
	fmt.Scan(&numberInput)

	//	looping numberInput

	for i := 1; i < numberInput; i++ {
		if (i%3 == 0) && (i%5 == 0) {
			result = append(result, "Kulina x Food")
		} else if i%3 == 0 {
			result = append(result, "Kulina")
		} else if i%5 == 0 {
			result = append(result, "Food")
		} else {
			result = append(result, strconv.Itoa(i))
		}
	}
	fmt.Printf("%s", result)
}
