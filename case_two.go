package main

import "fmt"

func main() {
	var firstString, secondString string
	fmt.Printf("Please input first string: ")
	fmt.Scanln(&firstString)
	fmt.Printf("Please input second string: ")
	fmt.Scanln(&secondString)

	for _, j := range firstString {
		for x, y := range secondString {
			if string(j) == string(y) {
				secondString = stringRemoveAt(secondString, x, 1)
				break
			}
		}
	}
	fmt.Println(secondString)
}

func stringRemoveAt(s string, index, length int) string {
	return s[:index] + s[index+length:]
}
