package main

import "fmt"

var roman = map[string]int{
	"I": 1,
	"V": 5,
	"X": 10,
	"L": 50,
	"C": 100,
	"D": 500,
	"M": 1000,
}

func main() {
	var inputRoman string
	var number, result, tempNumber int

	fmt.Printf("Please input your roman: ")
	fmt.Scanln(&inputRoman)

	for i := 0; i < len(inputRoman); i++ {
		char := inputRoman[len(inputRoman)-(i+1) : len(inputRoman)-i]
		number = roman[char]

		if number < tempNumber {
			result = result - number
			//fmt.Println(result)
		} else {
			result = result + number
			//fmt.Println(result)
		}

		tempNumber = number
	}
	fmt.Println(result)
}
